import { Component } from '@angular/core';
import { NavController, LoadingController, ToastController, AlertController} from 'ionic-angular';
import { CorekProvider } from '../../providers/corek/corek';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  form: FormGroup;
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  public show_option:Boolean = false;
  public user_id: any;
  public allActivities:any[] = [
    'Activo',
    'No activo',
    'Almorzando',
    'En el baño',
    'Reunido',
    'Permiso',
    'Recreacion'
  ];
  constructor(public navCtrl: NavController, public corek:CorekProvider, public formBuilder: FormBuilder, public toastCtrl:ToastController, private storage:Storage) {
    this.form = formBuilder.group({
      user: ['', Validators.required],
      password: ['', Validators.required],
    });

    this.storage.get('ID').then(id=>{
      this.user_id = id;
    });
    console.log(this.user_id);
  }

  ionViewDidLoad(){
    console.log('entro');
    let n = Date.now().toString+'cone'+Math.random();
    this.corek.ConnectCorekconfig(n);
    this.corek.socket.on(n, (dat, key)=>{
      console.log(dat);
    });
  }

  loginUser(){
    console.log(this.form.value.user)
    let nf = Date.now().toString()+'createuser';
    //this.corek.socket.emit('query',{'querystring':"SELECT * FROM `wp_users` WHERE `user_login` = '"+this.form.value.user+"'", 'event':nf});
    this.corek.socket.emit('get_users', {'event':nf,'condition':{'user_login':this.form.value.user}}); 
    this.corek.socket.on(nf, (users)=>{
      console.log(users)
      if (users.length > 0){
        let userdata = users[0];
        let event = Date.now()+'getLogin'+Math.random();
        this.corek.socket.emit('signon' , {log:this.form.value.user, pwd: this.form.value.password,'event':event, 'key':{'key':'login', 'datauser':userdata}});
        this.corek.socket.on(event, (response)=>{
          console.log(response)
          if(response.token != undefined){
            this.user_id = response.ID;
            this.storage.set('ID', response.ID)
          }else{
            let toast = this.toastCtrl.create({
              message: 'Verifica tu contraseña',
              duration: 5000
            });
            toast.present();
          }
        })
      }else{
        let toast = this.toastCtrl.create({
          message: 'Usuario no registrado',
          duration: 5000
        });
        toast.present();
      }
    });
  }

  showHide(){
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye' ? 'eye' : 'eye-off';
  }

  onChange(e){
    let now = new Date();
    let date = now.getFullYear()+'-'+(now.getMonth()+1)+'-'+now.getDate()+' '+now.getHours()+':'+now.getMinutes()+':'+now.getSeconds();
    let innsertAct = Date.now().toString()+"insercion"+Math.random();
    this.corek.socket.emit('insert_post',{'condition':{'post_type':'actividades','post_author':this.user_id, 'post_date': date,'post_content':e },'event':innsertAct});
    this.corek.socket.on(innsertAct, (result, key)=>{
      let toast = this.toastCtrl.create({
        message: 'Estatus actualizado',
        duration: 5000
      });
      toast.present();
    });
  };

  logout(){
    this.storage.clear();
    let toast = this.toastCtrl.create({
      message: 'Cerraste sesión',
      duration: 5000
    });
    toast.present();
    this.navCtrl.setRoot(this.navCtrl.getActive().component);
  }

}
