
import { Injectable } from '@angular/core';
import * as io from "socket.io-client";

/*
  Generated class for the CorekProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CorekProvider {

  socketHost: string = "https://v2.corek.io:8095";
  socket: any;

  constructor() {}
  
  public ConnectCorek(){
    this.socket = io.connect(this.socketHost,{'reconnection':true});
  }

  public ConnectCorekconfig(nf){
    this.socket = io.connect(this.socketHost,{'reconnection':true});// esto hace que se conecte al servidor (como cable directo)
    this.socket.on('connection', (data)=>{
      this.socket.emit('conf', { 'project': 'http://watcher.com','event':nf});
    });
  }

}
